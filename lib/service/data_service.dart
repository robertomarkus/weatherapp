// ignore_for_file: avoid_print

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:weatherapp/model/weather.dart';

class DataServices {
  Future<Weather> fetchData(String cityName) async {
    try {
      // api.openweathermap.org/data/2.5/weather?q={city name}&appid={API key}
      final queryParameters = {
        'q': cityName,
        'appid': 'f1c9cd79b35a6b41d61ec52cf5fe3d6d',
        'units': 'metric'
      };
      final uri = Uri.https(
          'api.openweathermap.org', '/data/2.5/weather', queryParameters);
      final response = await http.get(uri);
      if (response.statusCode == 200) {
        print(response.body);
        return Weather.fromJson(jsonDecode(response.body));
      } else {
        throw Exception('Failed to load Weather data');
      }
    } catch (e) {
      rethrow;
    }
  }
}
