import 'package:flutter/material.dart';
import 'package:weatherapp/page/mainpage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MainPage("string"),
      // home: InputPage(),
    );
  }
}

// class MainPage extends StatefulWidget {
//   const MainPage({
//     Key? key,
//   }) : super(key: key);

//   @override
//   State<MainPage> createState() => _MainPageState();
// }

// class _MainPageState extends State<MainPage> {
//   TextEditingController controller = TextEditingController();
//   DataServices dataServices = DataServices();
//   Weather weather = Weather();
//   bool isFetch = false;

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: const Text("Weather App"),
//       ),
//       body: Center(
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: [
//             isFetch
//                 ? Column(
//                     children: [
//                       Image.network(
//                           'http://openweathermap.org/img/wn/${weather.icon}@2x.png'),
//                       Text(
//                         '${weather.temp}°C',
//                         style: Theme.of(context).textTheme.headline2,
//                       ),
//                       Text(
//                         weather.description,
//                         style: Theme.of(context).textTheme.headline4,
//                       )
//                     ],
//                   )
//                 : const SizedBox(),
//             Container(
//               width: 150,
//               padding: const EdgeInsets.symmetric(vertical: 50),
//               child: TextField(
//                 controller: controller,
//                 textAlign: TextAlign.center,
//                 decoration: const InputDecoration(labelText: "Kota"),
//               ),
//             ),
//             ElevatedButton(
//                 onPressed: () async {
//                   isFetch = true;
//                   weather = await dataServices.fetchData(controller.text);
//                   setState(() {});
//                   // print(response.name);
//                   // print(response.description);
//                   // print(response.icon);
//                   // print(response.temp);
//                 },
//                 child: const Text("Search"))
//           ],
//         ),
//       ),
//     );
//   }
// }
