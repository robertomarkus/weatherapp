class Weather {
  final String name;
  final String description;
  final String icon;
  final double temp;
  final int humidity;
  final num pressure;
  final int clouds;
  final double wind;

  Weather({
    this.name = '',
    this.description = '',
    this.icon = '',
    this.temp = 0,
    this.humidity = 0,
    this.pressure = 0,
    this.clouds = 0,
    this.wind = 0,
  });

  factory Weather.fromJson(Map<String, dynamic> json) {
    return Weather(
      name: json['name'] ?? '',
      description: json['weather'][0]['description'] ?? '',
      icon: json['weather'][0]['icon'] ?? '',
      temp: json['main']['temp'] ?? 0,
      humidity: json['main']['humidity'] ?? 0,
      pressure: json['main']['pressure'] ?? 0,
      clouds: json['clouds']['all'] ?? 0,
      wind: json['wind']['speed'] ?? 0,
    );
  }
}

/*                        
// https://openweathermap.org/current 
// JSON Example of API Response

{
  "coord": {
    "lon": -122.08,
    "lat": 37.39
  },
  "weather": [
    {
      "id": 800,
      "main": "Clear",
      "description": "clear sky",
      "icon": "01d"
    }
  ],
  "base": "stations",
  "main": {
    "temp": 282.55,
    "feels_like": 281.86,
    "temp_min": 280.37,
    "temp_max": 284.26,
    "pressure": 1023,
    "humidity": 100
  },
  "visibility": 10000,
  "wind": {
    "speed": 1.5,
    "deg": 350
  },
  "clouds": {
    "all": 1
  },
  "dt": 1560350645,
  "sys": {
    "type": 1,
    "id": 5122,
    "message": 0.0139,
    "country": "US",
    "sunrise": 1560343627,
    "sunset": 1560396563
  },
  "timezone": -25200,
  "id": 420006353,
  "name": "Mountain View",
  "cod": 200
  }                         

                        
 */
