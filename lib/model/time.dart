class Time {
  final String icon;
  final double temp;
  final int cnt;

  Time({
    this.icon = '',
    this.temp = 0,
    this.cnt = 0,
  });

  factory Time.fromJson(Map<String, dynamic> json) {
    return Time(
      icon: json['list'][0]['weather'][0]['icon'] ?? '',
      temp: json['list'][0]['main']['temp'] ?? 0,
      cnt: json['cnt'] ?? 0,
    );
  }
}
