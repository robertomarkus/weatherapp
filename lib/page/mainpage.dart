// ignore_for_file: avoid_print
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:weatherapp/model/weather.dart';
import 'package:weatherapp/model/time.dart';
import 'package:weatherapp/service/data_service.dart';
import 'package:weatherapp/service/time_service.dart';

class MainPage extends StatefulWidget {
  final String kota;
  const MainPage(
    this.kota, {
    Key? key,
  }) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  TextEditingController controller = TextEditingController();
  TextEditingController nama = TextEditingController();
  DataServices dataServices = DataServices();
  TimeServices timeServices = TimeServices();
  Weather weather = Weather();
  Time time = Time();
  bool isFetch = false;

  @override
  // void initState() {
  // ignore: todo
  //   // TODO: implement initState
  //   super.initState();
  //   get();
  // }

  // void get() async {
  //   await dataServices.fetchData(widget.kota);
  // }

  @override
  Widget build(BuildContext context) {
    // print(widget.kota);
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text("Weather App"),
      // ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            isFetch
                ? Container(
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            IconButton(
                              onPressed: () {},
                              icon: const Icon(
                                Icons.arrow_back,
                                size: 30,
                                color: Colors.black,
                              ),
                            ),
                            Column(
                              children: <Widget>[
                                Text(
                                  controller.text,
                                  style: const TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                                const SizedBox(
                                  height: 5,
                                ),
                                Text(
                                  DateFormat.yMMMMEEEEd()
                                      .format(DateTime.now()),
                                  style: const TextStyle(
                                      fontWeight: FontWeight.w500,
                                      color: Colors.black),
                                ),
                              ],
                            ),
                            IconButton(
                              onPressed: () async {
                                isFetch = true;
                                weather = await dataServices
                                    .fetchData(controller.text);
                                setState(() {});

                                print(nama.text);
                                print(controller.text);
                              },
                              icon: const Icon(
                                Icons.refresh,
                                size: 30,
                                color: Colors.black,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                const SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    const Text(
                                      "Selamat Malam, ",
                                      style: TextStyle(
                                        fontWeight: FontWeight.w500,
                                        fontSize: 20,
                                      ),
                                    ),
                                    Text(
                                      nama.text,
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  '${weather.temp}°C',
                                  // style: Theme.of(context).textTheme.headline2,
                                  style: const TextStyle(
                                      fontSize: 50,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                const SizedBox(
                                  height: 40,
                                ),
                                Text(
                                  weather.description,
                                  // style: Theme.of(context).textTheme.headline4,
                                  style: const TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.w500),
                                ),
                                Image.network(
                                    'http://openweathermap.org/img/wn/${weather.icon}@2x.png'),
                              ],
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Column(
                              children: [
                                Image.network(
                                  'https://uxwing.com/wp-content/themes/uxwing/download/27-weather/drop-humidity.png',
                                  fit: BoxFit.fitWidth,
                                  height: 30,
                                ),
                                Text(
                                  '${weather.humidity}°C',
                                  // "text",

                                  style: const TextStyle(
                                    fontSize: 15,
                                    color: Colors.black,
                                  ),
                                ),
                                const Text(
                                  "Humidity",
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Column(
                              children: [
                                Image.network(
                                  'https://cdn-icons-png.flaticon.com/512/62/62848.png',
                                  fit: BoxFit.fitWidth,
                                  height: 30,
                                ),
                                Text(
                                  '${weather.pressure} Hpa',
                                  // "text",

                                  style: const TextStyle(
                                    fontSize: 15,
                                    color: Colors.black,
                                  ),
                                ),
                                const Text(
                                  "Pressure",
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Column(
                              children: [
                                Image.network(
                                  'https://uxwing.com/wp-content/themes/uxwing/download/27-weather/cloudy.png',
                                  fit: BoxFit.fitWidth,
                                  height: 30,
                                ),
                                Text(
                                  '${weather.clouds}%',
                                  // "text",

                                  style: const TextStyle(
                                    fontSize: 15,
                                    color: Colors.black,
                                  ),
                                ),
                                const Text(
                                  "Cloudiness",
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                            const SizedBox(
                              width: 20,
                            ),
                            Column(
                              children: [
                                Image.network(
                                  'https://uxwing.com/wp-content/themes/uxwing/download/27-weather/wind.png',
                                  fit: BoxFit.fitWidth,
                                  height: 30,
                                ),
                                Text(
                                  '${weather.wind} m/s',
                                  // "text",

                                  style: const TextStyle(
                                    fontSize: 15,
                                    color: Colors.black,
                                  ),
                                ),
                                const Text(
                                  "Wind",
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 30,
                        ),
                        //time
                        Container(
                          height: 200,
                          decoration: BoxDecoration(
                            color: Colors.black12,
                            borderRadius: BorderRadius.circular(20),
                          ),
                          margin: const EdgeInsets.only(left: 10, right: 10),
                          child: ListView(
                            padding: const EdgeInsets.only(bottom: 20, top: 20),
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Column(
                                    children: const <Widget>[
                                      Text(
                                        "Wed",
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Text(
                                        "4/13",
                                        style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      const Text(
                                        "00:00",
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.black,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Image.network(
                                        'http://openweathermap.org/img/wn/${weather.icon}@2x.png',
                                        fit: BoxFit.fitWidth,
                                        height: 25,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "${weather.temp} °C",
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      const Text(
                                        "03:00",
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.black,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Image.network(
                                        'http://openweathermap.org/img/wn/${weather.icon}@2x.png',
                                        fit: BoxFit.fitWidth,
                                        height: 25,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "${weather.temp} °C",
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      const Text(
                                        "06:00",
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.black,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Image.network(
                                        'http://openweathermap.org/img/wn/${weather.icon}@2x.png',
                                        fit: BoxFit.fitWidth,
                                        height: 25,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "${weather.temp} °C",
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      const Text(
                                        "09:00",
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.black,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Image.network(
                                        'http://openweathermap.org/img/wn/${weather.icon}@2x.png',
                                        fit: BoxFit.fitWidth,
                                        height: 25,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "${weather.temp} °C",
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      const Text(
                                        "12:00",
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.black,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Image.network(
                                        'http://openweathermap.org/img/wn/${weather.icon}@2x.png',
                                        fit: BoxFit.fitWidth,
                                        height: 25,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "${weather.temp} °C",
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              const SizedBox(
                                height: 40,
                              ),
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                children: [
                                  Column(
                                    children: const <Widget>[
                                      Text(
                                        "Thu",
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.black,
                                        ),
                                      ),
                                      Text(
                                        "4/14",
                                        style: TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      const Text(
                                        "00:00",
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.black,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Image.network(
                                        'http://openweathermap.org/img/wn/${weather.icon}@2x.png',
                                        fit: BoxFit.fitWidth,
                                        height: 25,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "${weather.temp} °C",
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      const Text(
                                        "03:00",
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.black,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Image.network(
                                        'http://openweathermap.org/img/wn/${weather.icon}@2x.png',
                                        fit: BoxFit.fitWidth,
                                        height: 25,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "${weather.temp} °C",
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      const Text(
                                        "06:00",
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.black,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Image.network(
                                        'http://openweathermap.org/img/wn/${weather.icon}@2x.png',
                                        fit: BoxFit.fitWidth,
                                        height: 25,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "${weather.temp} °C",
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      const Text(
                                        "09:00",
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.black,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Image.network(
                                        'http://openweathermap.org/img/wn/${weather.icon}@2x.png',
                                        fit: BoxFit.fitWidth,
                                        height: 25,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "${weather.temp} °C",
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                  Column(
                                    children: [
                                      const Text(
                                        "12:00",
                                        style: TextStyle(
                                          fontSize: 10,
                                          color: Colors.black,
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Image.network(
                                        'http://openweathermap.org/img/wn/${weather.icon}@2x.png',
                                        fit: BoxFit.fitWidth,
                                        height: 25,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        "${weather.temp} °C",
                                        style: const TextStyle(
                                          fontSize: 10,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  )
                : const SizedBox(),
            //test
            const SizedBox(
              height: 30,
            ),

            //test
            Container(
              width: 150,
              padding: const EdgeInsets.symmetric(vertical: 50),
              child: Column(
                children: [
                  TextField(
                    controller: nama,
                    textAlign: TextAlign.center,
                    decoration: const InputDecoration(labelText: "Nama"),
                  ),
                  TextField(
                    controller: controller,
                    textAlign: TextAlign.center,
                    decoration: const InputDecoration(labelText: "Kota"),
                  ),
                ],
              ),
            ),
            ElevatedButton(
                onPressed: () async {
                  isFetch = true;
                  weather = await dataServices.fetchData(controller.text);
                  setState(() {});
                  // print(response.name);
                  // print(response.description);
                  // print(response.icon);
                  // print(response.temp);
                  print(nama.text);
                  print(controller.text);
                },
                child: const Text("Search"))
          ],
        ),
      ),
    );
  }
}
