// ignore_for_file: avoid_print

import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  const MainPage(String s, {Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(
        //   backgroundColor: Colors.transparent,
        //   // backgroundColor: const Color(0x44000000),
        //   elevation: 0,
        //   title: const Text(""),
        // ),
        body: Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image:
              const NetworkImage('https://wallpapercave.com/wp/wp4828112.jpg'),
          fit: BoxFit.cover,
          colorFilter:
              ColorFilter.mode(Colors.black.withOpacity(0.2), BlendMode.darken),
        ),
      ),
      // margin: const EdgeInsets.all(15),
      child: Column(
        children: [
          const SizedBox(
            height: 90,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.arrow_back,
                  size: 30,
                  color: Colors.white,
                ),
              ),
              Column(
                children: const <Widget>[
                  Text(
                    "Bandung",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "Thursday, May 27 2022",
                    style: TextStyle(
                        fontWeight: FontWeight.w500, color: Colors.white),
                  ),
                ],
              ),
              IconButton(
                onPressed: () {},
                icon: const Icon(
                  Icons.refresh,
                  size: 30,
                  color: Colors.white,
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 30,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Column(
                children: <Widget>[
                  Row(
                    children: const [
                      Text(
                        "Selamat [Sore], ",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.w500,
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        "[Markro]",
                        style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  const Text(
                    "28.33 °C",
                    style: TextStyle(
                      fontSize: 50,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  )
                ],
              ),
              const SizedBox(
                width: 20,
              ),
              Column(
                children: <Widget>[
                  const Text(
                    "Cloudy",
                    style: TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    ),
                  ),
                  // Text("Cloudy"),
                  Image.network(
                    'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                    fit: BoxFit.fitWidth,
                    height: 75,
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 20,
          ),
          Container(
            padding: const EdgeInsets.all(15.0),
            decoration: BoxDecoration(
              color: Colors.black12,
              borderRadius: BorderRadius.circular(20),
            ),
            margin: const EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Column(
                  children: [
                    Image.network(
                      'https://uxwing.com/wp-content/themes/uxwing/download/27-weather/drop-humidity.png',
                      fit: BoxFit.fitWidth,
                      height: 30,
                    ),
                    const Text(
                      "58%",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                      ),
                    ),
                    const Text(
                      "Humidity",
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Image.network(
                      'https://cdn-icons-png.flaticon.com/512/62/62848.png',
                      fit: BoxFit.fitWidth,
                      height: 34,
                    ),
                    const Text(
                      "1009 Hpa",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                      ),
                    ),
                    const Text(
                      "Pressure",
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Image.network(
                      'https://uxwing.com/wp-content/themes/uxwing/download/27-weather/cloudy.png',
                      fit: BoxFit.fitWidth,
                      height: 33,
                    ),
                    const Text(
                      "87%",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                      ),
                    ),
                    const Text(
                      "Cloudiness",
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                Column(
                  children: [
                    Image.network(
                      'https://uxwing.com/wp-content/themes/uxwing/download/27-weather/wind.png',
                      fit: BoxFit.fitWidth,
                      height: 30,
                    ),
                    const Text(
                      "2.14 m/s",
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                      ),
                    ),
                    const Text(
                      "Wind",
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            height: 430,
            decoration: BoxDecoration(
              color: Colors.black26,
              borderRadius: BorderRadius.circular(20),
            ),
            margin: const EdgeInsets.all(10),
            child: ListView(
              padding: const EdgeInsets.only(bottom: 20, top: 20),
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: const <Widget>[
                        Text(
                          "Fri",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          "5/28",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: const <Widget>[
                        Text(
                          "Fri",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          "5/29",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: const <Widget>[
                        Text(
                          "Fri",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          "5/30",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: const <Widget>[
                        Text(
                          "Fri",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          "5/31",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: const <Widget>[
                        Text(
                          "Fri",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          "6/01",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
                const SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: const <Widget>[
                        Text(
                          "Fri",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        Text(
                          "6/02",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        const Text(
                          "00:00",
                          style: TextStyle(
                            fontSize: 10,
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Image.network(
                          'https://cdn-icons-png.flaticon.com/512/164/164806.png',
                          fit: BoxFit.fitWidth,
                          height: 25,
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        const Text(
                          "21.99 °C",
                          style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    ));
  }
}

//asdasdsadasdasdasdsa
// const SizedBox(
//   height: 10,
// ),
// Row(
//   mainAxisAlignment: MainAxisAlignment.spaceBetween,
//   children: <Widget>[
//     Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: const [
//         Text(
//           "Selamat [Pagi]",
//           style: TextStyle(
//             fontSize: 21,
//             fontWeight: FontWeight.w500,
//           ),
//         ),
//         Text(
//           "Markro",
//           style: TextStyle(
//             fontSize: 20,
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//         SizedBox(
//           height: 20,
//         ),
//         Text(
//           "28.33 °C",
//           style: TextStyle(
//             fontSize: 40,
//             fontWeight: FontWeight.bold,
//           ),
//         ),
//       ],
//     ),
//     Column(
//       crossAxisAlignment: CrossAxisAlignment.end,
//       children: <Widget>[
//         const Text(
//           "Cloudy",
//           style: TextStyle(
//             fontSize: 21,
//             fontWeight: FontWeight.w700,
//           ),
//         ),
//         const SizedBox(
//           height: 10,
//         ),
//         Container(
//           child: Image.network(
//             'https://cdn-icons-png.flaticon.com/512/164/164806.png',
//             fit: BoxFit.fitWidth,
//             height: 75,
//           ),
//         ),
//       ],
//     ),
//   ],
// ),
