// ignore_for_file: avoid_print, duplicate_ignore

import 'package:flutter/material.dart';
import 'package:snippet_coder_utils/FormHelper.dart';
import 'package:weatherapp/model/weather.dart';
import 'package:weatherapp/page/mainpage.dart';
import 'package:weatherapp/service/data_service.dart';

class InputPage extends StatefulWidget {
  const InputPage({Key? key}) : super(key: key);

  @override
  State<InputPage> createState() => _InputPageState();
}

class _InputPageState extends State<InputPage> {
  TextEditingController controller = TextEditingController();
  DataServices dataServices = DataServices();
  Weather weather = Weather();
  bool isFetch = false;

  List<dynamic> provinsi = [];
  List<dynamic> kotaMasters = [];
  List<dynamic> kota = [];

  String? provinsiId;
  String? kotaId;

  @override
  void initState() {
    super.initState();

    provinsi.add({"IDprovinsi": 1, "label": "Jawa Barat"});
    provinsi.add({"IDprovinsi": 2, "label": "Jawa Tengah"});
    provinsi.add({"IDprovinsi": 3, "label": "Jawa Timur"});

    kotaMasters = [
      {"IDkota": 1, "Namakota": "Depok", "IDprovinsi": 1},
      {"IDkota": 2, "Namakota": "Bogor", "IDprovinsi": 1},
      {"IDkota": 3, "Namakota": "Bekasi", "IDprovinsi": 1},
      {"IDkota": 1, "Namakota": "Cilacap", "IDprovinsi": 2},
      {"IDkota": 2, "Namakota": "Banyumas", "IDprovinsi": 2},
      {"IDkota": 3, "Namakota": "Purbalingga", "IDprovinsi": 2},
      {"IDkota": 1, "Namakota": "Bangkalan", "IDprovinsi": 3},
      {"IDkota": 2, "Namakota": "Banyuwangi", "IDprovinsi": 3},
      {"IDkota": 3, "Namakota": "Blitar", "IDprovinsi": 3},
    ];
  }

  @override
  Widget build(BuildContext context) {
    // ignore: avoid_print
    // print("Build Screen");
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Input Form"),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: 150,
              // padding: const EdgeInsets.symmetric(vertical: 10),
              child: TextField(
                controller: controller,
                textAlign: TextAlign.center,
                decoration:
                    const InputDecoration(labelText: "Masukan Nama Anda"),
              ),
            ),
            FormHelper.dropDownWidgetWithLabel(
              context,
              "Provinsi",
              "Pilih Provinsi",
              provinsiId,
              provinsi,
              (onChangeVal) {
                provinsiId = onChangeVal;
                // ignore: avoid_print
                //print("Provinsi yang dipilih: $onChangeVal");

                kota = kotaMasters
                    .where(
                      (stateItem) =>
                          stateItem["IDprovinsi"].toString() ==
                          onChangeVal.toString(),
                    )
                    .toList();
                // print(kota.map((e) => e).toList());
                kotaId = null;
                // print(kotaId);
                setState(() {});
              },
              (onValidateVal) {
                if (onValidateVal == null) {
                  return "Please Select Provinsi";
                }
                return null;
              },
              borderColor: Theme.of(context).primaryColor,
              borderFocusColor: Theme.of(context).primaryColor,
              borderRadius: 10,
              optionLabel: "label",
              optionValue: "IDprovinsi",
            ),
            FormHelper.dropDownWidgetWithLabel(
              context,
              "Kota",
              "Pilih Kota",
              kotaId,
              kota,
              (String onChangedVal) {
                // kotaId = onChangedVal;
                // ignore: avoid_print
                kotaId = onChangedVal;
                // print(kotaId);
                // print(kota.map((e) => e).toList());
                if (kotaId == "1" && provinsiId == "1") {
                  kotaId = "Depok";
                }
                if (kotaId == "2" && provinsiId == "1") {
                  kotaId = "Bogor";
                }
                if (kotaId == "3" && provinsiId == "1") {
                  kotaId = "Bekasi";
                }

                if (kotaId == "1" && provinsiId == "2") {
                  kotaId = "Cilacap";
                }
                if (kotaId == "2" && provinsiId == "2") {
                  kotaId = "Banyumas";
                }
                if (kotaId == "3" && provinsiId == "2") {
                  kotaId = "Purbalingga";
                }

                if (kotaId == "1" && provinsiId == "3") {
                  kotaId = "Bangkalan";
                }
                if (kotaId == "2" && provinsiId == "3") {
                  kotaId = "Banyuwangi";
                }
                if (kotaId == "3" && provinsiId == "3") {
                  kotaId = "Blitar";
                }
                // print(kotaId);
                //print("Pilih Kota: $onChangedVal");
              },
              (String onValidate) {
                return null;
              },
              borderColor: Theme.of(context).primaryColor,
              borderFocusColor: Theme.of(context).primaryColor,
              borderRadius: 10,
              optionLabel: "Namakota",
              optionValue: "IDkota",
            ),
            const SizedBox(
              height: 10,
            ),
            ElevatedButton(
              onPressed: () async {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MainPage(kotaId!)),
                );
                setState(() {});
                print(kotaId);
                print(controller.text);
              },
              child: const Padding(
                padding:
                    EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
                child: Text(
                  "Proses",
                  style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                ),
              ),
              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(25),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
